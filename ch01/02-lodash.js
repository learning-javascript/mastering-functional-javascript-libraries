const _ = require('lodash');

console.log('1)', _.filter(_.map([1, 2, 3], x => x * 2), x => x > 3));
console.log('2)', _([1, 2, 3]).map(x => x * 2).filter(x => x > 3).value());
console.log('3)', _([1, 2, 3]).map(x => x * 2).filter(x => x > 3).first());
console.log('4)', _([1, 2, 3]).map(x => x * 2).filter(x => x > 3).take(1).value());

_([1, 2, 3]).map(x => {
  console.log('map', x);
  return x * 2;
}).filter(x => {
  console.log('filter', x);
  return x > 3;
}).first();

console.log('5)', _([1, 2, 3]).map(x => x * 2).reduce((a, b) => a * b, 1));

// Cloning
const domNode = {
  className: 'js-container',
  tagName: 'div',
  childNodes: [
    {
      text: 'H1'
    }
  ]
};

const cloneNode = _.cloneDeep(domNode);
cloneNode.childNodes[0].text = 'Hello';

console.log('6)', domNode.childNodes[0], cloneNode.childNodes[0]);

const left = {};
left.right = {};
left.right.left = left;

console.log('7)', _.cloneDeep(left));

// Currying and partial implementation
let getParamsString = (params = {}) =>
  Object.entries(params).reduce((acc, param) => `${acc}&${param[0]}=${param[1]}`, '');

let openWeatherMapUrl =
  endpoint =>
    apiKey =>
      params =>
        city => `http://api.openweathermap.org/data/2.5/${endpoint}?q=${city}&appid=${apiKey}${getParamsString(params)}`;

console.log('8)', openWeatherMapUrl('endpoint')('KEY')({param: 'value'})('Zurich'));

openWeatherMapUrl =
  (endpoint, apiKey, params, city) =>
    `http://api.openweathermap.org/data/2.5/${endpoint}?q=${city}&appid=${apiKey}${getParamsString(params)}`;

console.log('9)', openWeatherMapUrl('endpoint', 'KEY', {param: 'value'}, 'Zurich'));

console.log('10)', _.curry(openWeatherMapUrl)('endpoint')('KEY')({param: 'value'})('Zurich'));
console.log('11)', _.curry(openWeatherMapUrl)('endpoint')('KEY')({param: 'value'}, 'Zurich'));
console.log('12)', _.curryRight(openWeatherMapUrl)('Zurich')({param: 'value'})('KEY')('endpoint'));

const getCityWeather = _.partial(openWeatherMapUrl, 'endpoint', 'KEY', {});
console.log('13)', getCityWeather('Zurich'));
