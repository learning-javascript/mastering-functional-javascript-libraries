const _ = require('underscore');

console.log('1)', _.map([1, 2, 3], x => x * 2));
console.log('2)', _([1, 2, 3]).map(x => x * 2));

console.log('3)', _.filter(_.map([1, 2, 3], x => x * 2), x => x > 3));
console.log('4)', _([1, 2, 3]).map(x => x * 2).filter(x => x > 3));

const courses = [
  {
    title:'Deep Dive into Functional Programming in JavaScipt',
    author: 'Zsolt Nagy',
    sections: 6
  },
  {
    title:'Implementing and Testing JavaScript Application using Functional Programming',
    author: 'Zsolt Nagy',
    sections: 5
  },
  {
    title:'Mastering JavaScript Functional Programming Libraries',
    author: 'Zsolt Nagy',
    sections: 3
  }
];

const tableTemplate = `
    <table>
        <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Sections</th>
            </tr>
        </thead>
        <tbody>
            <% _.each(courses, course => { %>
                <tr>
                    <td><%= course.title %></td>
                    <td><%= course.author %></td>
                    <td><%= course.sections %></td>
                </tr>
            <% }); %>
        </tbody>
    </table>
`;

console.log(_.template(tableTemplate)({courses}));
