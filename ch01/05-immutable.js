const user = {
  name: 'Edu'
};

user.name = 'Arno';
user.age = 27;
console.log('1)', user);

Object.freeze(user);
user.name = 'Lauri';
console.log('2)', user);

const Immutable = require('immutable');

const firstCourse = Immutable.Map({
  title: 'Deep Dive into Functional Programming in JavaScipt',
  author: 'Zsolt Nagy',
  sections: 6
});
const firstCourse1 = firstCourse.set('author', 'Z.Nagy');
console.log('3)', firstCourse.get('author'), firstCourse1.get('author'));

console.log('4)', (firstCourse == firstCourse1));
console.log('5)', (firstCourse === firstCourse1));

const firstCourse2 = firstCourse1.set('author', 'Zsolt Nagy');
console.log('6)', (firstCourse == firstCourse2), (firstCourse === firstCourse2));

console.log('7)', firstCourse.equals(firstCourse2));
console.log('8)', firstCourse.equals(firstCourse1));

console.log('9)', firstCourse2.toObject());
console.log('10)', firstCourse2.toJSON());
console.log('11)', JSON.stringify(firstCourse2));

const eventList = Immutable.List(['click', 'scroll', 'keypress', 'click']);
const eventList2 = eventList.push('paste');

console.log('12)', eventList.size, eventList2.size);
console.log('13)', eventList2.get(0));
console.log('14)', eventList2.get(4));
console.log('15)', eventList2.get(5));

const eventList3 = eventList2.pop().shift();
console.log('16)', eventList3.toJSON());
console.log('17)', eventList3.toJS());
console.log('18)', eventList3.toArray());
console.log('19)', eventList3.toObject());

const businessDays = Immutable.List([
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday'
]);
const weekendDays = Immutable.List(['Saturday', 'Sunday']);
const weekDays = businessDays.concat(weekendDays);
console.log('20)', weekDays);

const weekDays2 = businessDays.concat(['Saturday', 'Sunday']);
console.log('21)', weekDays2);

const weekDays3 = weekDays.map(day => day.slice(0, 2));
console.log('22)', weekDays3);

const weekDays4 = weekDays
  .map(day => day.slice(0, 2))
  .filter(day => !day.startsWith('S'))
  .reduce((acc, day2) => `${acc} ${day2}`);
console.log('23)', weekDays4);

const countToHundredStepFive = Immutable.Range(0, 100, 5);
console.log('24)', countToHundredStepFive.toJSON());
console.log('25)', countToHundredStepFive.take(2).toJSON());

console.log('26)', Immutable.Repeat('ha').take(10).join(''));
console.log('27)', Immutable.Repeat('ha', 10).join(''));

console.log('28)', Immutable.Repeat().map(Math.random).take(10).toJS());
console.log('29)', Immutable.Repeat().map(Math.random).take(10000).reduce((a, b) => a + b));

function* fibonacci() {
  let fib1 = 1;
  let fib2 = 2;
  yield fib1;
  while(true) {
    yield fib2;
    [fib2, fib1] = [fib1 + fib2, fib2];
  }
}
const fibonacciIteraator = fibonacci();
const fibonacciSequence = Immutable.Seq(fibonacciIteraator);

console.log('30)', fibonacciSequence.filter(x => x % 2 === 0).take(20).toJS());

Immutable.Seq([
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday'
]).take(5).map(day => {
  console.log('map #2', day);
  return day.slice(0, 2);
}).reduce((acc, day) => {
  console.log('reduce #1', acc, day);
  return `${acc} ${day}`;
});

Immutable.Seq([
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday'
]).map(day => {
  console.log('map #2', day);
  return day.slice(0, 2);
}).take(5).reduce((acc, day) => {
  console.log('reduce #2', acc, day);
  return `${acc} ${day}`;
});
