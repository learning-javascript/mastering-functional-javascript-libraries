(ns clojurescript-samples.core
    (:require ))

(enable-console-print!)

(println (str "01) (* 2 4) = " (* 2 4)))

; this is anonymous function
#(* 2 %1)
(println (str "02) " (#(* 2 %1) 4)))

(defn mul2 [x] (* 2 x))
(println (str "03) " (mul2 4)))

(println (str "04) " (map mul2(array 1 2 3))))

(defn gt3 [x] (>= x 3))
(println (str "05) " (filter gt3 (map mul2(array 1 2 3)))))

;; define your app data so that it doesn't get over-written on reload

(defonce app-state (atom {:text "Hello world!"}))


(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
