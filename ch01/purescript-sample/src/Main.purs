module Main where

import Prelude
import Data.Array (filter)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, logShow)

mul2 :: Int -> Int
mul2 n = 2 * n

gt3 :: Int -> Boolean
gt3 n = n > 3

fact :: Int -> Int
fact 0 = 1
fact n = n * fact (n - 1)

main :: forall e. Eff (console :: CONSOLE | e) Unit
main = do
  logShow $ mul2 4
  logShow $ gt3 3
  logShow $ gt3 4
  logShow $ map mul2 [1, 2, 3]
  logShow $ filter gt3 (map mul2 [1, 2, 3])
  logShow $ filter (\n -> n > 3) (map (\n -> n * 2) [1, 2, 3])
  logShow $ fact 5
