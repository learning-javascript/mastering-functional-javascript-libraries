const R = require('ramda');

console.log('1)', R.map(x => x * 2, [1, 2, 3]));

const doubleEach = R.map(x => x * 2);

console.log('2)', doubleEach([1, 2, 3, 4, 5]));

console.log('3)', R.filter(x => x > 3, R.map(x => x * 2, [1, 2, 3])));
console.log('4)', R.filter(x => x > 3)(R.map(x => x * 2, [1, 2, 3])));

const mapFilter = R.compose(
  R.filter(x => x > 3),
  R.map(x => x * 2)
);
console.log('5)', mapFilter([1, 2, 3, 4, 5]));

const logMapFilter = R.pipe(
  R.map(x => {
    console.log('map', x);
    return x * 2;
  }),
  R.filter(x => {
    console.log('filter', x);
    return x > 3;
  }),
  R.take(2)
);

console.log('6)', logMapFilter([1, 2, 3, 4, 5]));

const transducer = R.compose(
  R.map(x => {
    console.log('map', x);
    return x * 2;
  }),
  R.filter(x => {
    console.log('filter', x);
    return x > 3;
  }),
  R.take(2)
);

console.log('7)', R.into([], transducer)([1, 2, 3, 4, 5]));

const fetch = require('node-fetch');
require('console.table');

fetch('https://api.github.com/users/facebook/repos')
  .then(resp => resp.json())
  .then(R.map(R.pick(['full_name', 'forks'])))
  .then(R.sortBy(R.prop('forks')))
  .then(console.table);

const pickNameForks = R.map(R.pick(['full_name', 'forks']));
const sortByForks = R.sortBy(R.prop('forks'));
fetch('https://api.github.com/users/facebook/repos')
  .then(resp => resp.json())
  .then(pickNameForks)
  .then(sortByForks)
  .then(console.table);

const formatData = R.pipe(pickNameForks, sortByForks);
fetch('https://api.github.com/users/facebook/repos')
  .then(resp => resp.json())
  .then(formatData)
  .then(console.table);
