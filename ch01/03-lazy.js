const Lazy = require('lazy.js');

console.log('1)', Lazy([1, 2, 3, 4]));
console.log('2)', Lazy('sequence of characters'));

const s = new Set([1, 2, 3]);
console.log('3)', Lazy(s));
console.log('4)', Lazy(s).take(3).value());
console.log('5)', Lazy(s).value());

Lazy([1, 2, 3]).map(x => {
  console.log('map', x);
  return x * 2;
}).filter(x => {
  console.log('filter', x);
  return x > 3;
}).first();

Lazy.makeHttpRequest('http://bnb.data.bl.uk/doc/resource/007446989.json')
  .lines()
  .take(10)
  .each(line => console.log(line));

let response = '';
const onFulfilled = () => console.log(JSON.parse(response));

Lazy.makeHttpRequest('http://bnb.data.bl.uk/doc/resource/007446989.json')
  .lines()
  .each(line => {
    response += line;
  }).then(onFulfilled);
