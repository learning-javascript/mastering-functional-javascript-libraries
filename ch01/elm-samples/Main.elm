import Html exposing (text)
import List exposing (map, filter)

main =
    text (
        toString (
            filter (\x -> x > 3) (map (\x -> x * 2) [1, 2, 3])
        )
    )
